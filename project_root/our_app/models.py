from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser
#from phonenumber_field.modelfields import PhoneNumberField
#from django.contrib.auth import get_user_model
from django.conf import settings


#User = get_user_model()
# Create your models here.
DEGREE = (
        ('Undergraduate', 'Undergraduate'),
        ('Masters', 'Masters'),
        ('Ph.D', 'Ph.D'),
        ('Post Doc', 'Post Doc'),
    )

class Country(models.Model):
    country_name = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.country_name

class ResearchField(models.Model):
    research_field = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.research_field



class Institution(models.Model):
    institution_name = models.CharField(max_length=50, null=True)
    institution_location = models.CharField(max_length=50, null=True)
    institution_category = models.CharField(max_length=50, null=True)

    def __str__(self):
        return str(self.id)

class Designation(models.Model):
    designation = models.CharField(max_length=50, null=True)

    def __str__(self):
        return str(self.id)

class Position(models.Model):
    position = models.CharField(max_length=50, null=True)

    def __str__(self):
        return str(self.id)

class Skill(models.Model):
    skill = models.CharField(max_length=50, null=True)

    def __str__(self):
        return str(self.id)



class FieldOfStudy(models.Model):
    major = models.CharField(max_length=50, null=True)

    def __str__(self):
        return str(self.id)



class Student(models.Model):
    #FK from other tables
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE, unique=True)
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.CASCADE) #present country
    #fields = models.ForeignKey(ResearchField, blank=True, null=True, on_delete=models.SET_NULL)
    research_field = models.ManyToManyField(ResearchField)
    #publication = models.ManyToManyField(Publication, blank=True, null=True)
    #education = models.OneToOneField(Education, on_delete=models.CASCADE)
    skill = models.ManyToManyField(Skill)


    #own fields
    intended_position = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=200, null=True)
    research_interest = models.TextField(null=True)
    email = models.CharField(max_length=200, null=False)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)


class Education(models.Model):
    # FK from other tables
    field_of_study = models.ForeignKey(FieldOfStudy, blank=True, null=True, on_delete=models.CASCADE)
    institution = models.ForeignKey(Institution, blank=True, null=True, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, blank=True, null=True, on_delete=models.CASCADE)

    # own fields
    degree = models.CharField(max_length=200, null=True)
    from_date = models.DateField(null=True)
    to_date = models.DateField(null=True)
    grade = models.FloatField(null=True)
    description = models.CharField(max_length=200, null=True)

    def __str__(self):
        return str(self.id)

class Experience(models.Model):
    # FK from other table
    student = models.ForeignKey(Student, blank=True, null=True, on_delete=models.CASCADE)

    # own fields
    title = models.CharField(max_length=50, null=True)
    description = models.TextField(null=True)
    from_date = models.DateField(null=True)
    to_date = models.DateField(null=True)

    def __str__(self):
        return str(self.id)

class Professor(models.Model):
    # FK from other tables
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE)
    institution = models.ForeignKey(Institution, blank=True, null=True, on_delete=models.CASCADE)
    designation = models.ForeignKey(Designation, blank=True, null=True, on_delete=models.CASCADE)

    field = models.ManyToManyField(ResearchField)
    #field = models.ForeignKey(ResearchField, blank=True, null=True, on_delete=models.SET_NULL)

    #publication = models.ManyToManyField(Publication)

    # own fields
    research_interest = models.CharField(max_length=200, null=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)

class Publication(models.Model):
    #FK from other tables
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False, on_delete=models.CASCADE)

    #own fields
    title = models.CharField(max_length=200, null=True)
    year_published = models.DateField(null=True)
    publisher = models.CharField(max_length=50, null=True)
    url = models.CharField(max_length=100, null=True)
    description = models.TextField(null=True)

    def __str__(self):
        return str(self.id)

class Offer(models.Model):
    # FK from other tables
    institution = models.ForeignKey(Institution, blank=True, null=True, on_delete=models.SET_NULL)
    position = models.ForeignKey(Position, blank=True, null=True, on_delete=models.SET_NULL)
    professor = models.ForeignKey(Professor, blank=True, null=True, on_delete=models.SET_NULL)

    # own fields
    #position = models.CharField(max_length=200, null=True,choices=DEGREE)
    major = models.CharField(max_length=200, null=True)
    min_cg_requirement = models.FloatField(null=True)
    #university = models.CharField(max_length=200, null=True)
    research_field = models.CharField(max_length=200, null=True)
    position_description = models.CharField(max_length=200, null=True)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id) + ', ' + str(self.position) + ', ' + str(self.major)


class StudentOfferInfo(models.Model):
    #FK from other tables
    student = models.OneToOneField(Student, on_delete=models.CASCADE)
    offer = models.ForeignKey(Offer, blank=True, null=True, on_delete=models.SET_NULL)

    #own field
    is_saved = models.BooleanField(default=False)
    is_applied = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


class CertificatName(models.Model):
    certificate_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return str(self.id)

class Certification(models.Model):
    # FK from other tables
    certificate_name = models.ForeignKey(CertificatName, blank=True, null=True, on_delete=models.SET_NULL)
    student = models.ForeignKey(Student, blank=True, null=True, on_delete=models.SET_NULL)

    #own fields
    score = models.CharField(max_length=100, null=True)

    def __str__(self):
        return str(self.id)

class User(AbstractUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        null=False,
    )

    role = models.CharField(max_length=30, blank=True)

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def __str__(self):
        return self.email


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    picture_social = models.CharField(max_length=200, blank=True)
    picture_local = models.ImageField(default='default_image.jpg')
    def __str__(self):
        return self.user.username + '_Profile'


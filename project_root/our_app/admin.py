from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Student)
admin.site.register(Professor)
admin.site.register(Offer)
admin.site.register(User)
admin.site.register(Country)
admin.site.register(ResearchField)
admin.site.register(Publication)
admin.site.register(Institution)
admin.site.register(Designation)
admin.site.register(Position)
admin.site.register(CertificatName)
admin.site.register(Certification)
admin.site.register(Profile)
admin.site.register(Skill)
admin.site.register(Experience)
admin.site.register(FieldOfStudy)
admin.site.register(Education)
admin.site.register(StudentOfferInfo)



from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User



ROLE_CHOICES = [
    ('professor', 'Professor'),
    ('student', 'Student'),
    ]

#SignupForm class to extend extra fields
class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    role = forms.CharField(max_length=30, required=False, help_text='Optional.')
    role = forms.CharField(label='Select a role',
                                     widget=forms.Select(choices=ROLE_CHOICES))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'role')



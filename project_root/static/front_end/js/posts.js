$(document).on('submit', '#overview_form', function(){
        //alert('worked')
        //url = '{% url "" %}'
        $.ajax({
            type:'POST',
            url:'/submit/',
            data:{
                overview:$('#overview_and_research').val(),
                position:$('#intended_position').val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            },
            success:function(){
                alert('inserted into database!');
                window.location.reload(true);
            }

        });

    });


$(document).on('submit', '#ajax_contact', function(){
    alert('worked')
    //url = '{% url "" %}'
    $.ajax({
        type:'POST',
        url:'/submit/contact_submit',
        data:{
            email:$('#email').val(),
            phone:$('#phone').val(),
            address:$('#address').val(),
            country:$('#country').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(){
            alert('inserted into database!');
            window.location.reload(true);
        }

    });

});


$(document).on('submit', '#ajax_skill', function(){
    alert('worked')
    //url = '{% url "" %}'
    $.ajax({
        type:'POST',
        url:'/submit/skill_submit',
        data:{
            skill_id:$('#student_skill').val(),
            new_skill:$('#new_skill').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(){
            alert('inserted into database!');
            window.location.reload(true);
        }

    });
});


function skill_delete(id) {
    //   alert(id)
    $.ajax({
        type:'GET',
        url:'/submit/skill_delete',
        data:{
            skill_id:id,
            // csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(){
            alert('Skill Deleted!');
            window.location.reload(true);
        }
    });
    }

$(document).on('click', '#save_experience', function(){
        //alert('worked')
        //url = '{% url "" %}'
        $.ajax({
            type:'POST',
            url:'/submit_experience/',
            data:{
               job_title:$('#job_title').val(),
               job_description:$('#job_description').val(),
               from_date:$('#from_date').val(),
               to_date:$('#to_date').val(),
               csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            },
            success:function(){
               alert('inserted into database!');
                window.location.reload(true);
            }

        });

    });

$(document).on('click', '#update_experience', function(){
        //alert($('#experience_id').val());
        //url = '{% url "" %}'
        $.ajax({
                type:'POST',
                url:'/update_experience/',
                data:{
                    id:$('#experience_id').val(),
                    job_title:$('#job_title').val(),
                    job_description:$('#job_description').val(),
                    from_date:$('#from_date').val(),
                    to_date:$('#to_date').val(),
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                },
                success:function(){
                   alert('Experience updated!');
                    window.location.reload(true);
                }
          });
});


$(document).on('click', '#save_education', function(){
        //alert($('#new_institution').val());
        //url = '{% url "" %}'
        $.ajax({
                type:'POST',
                url:'/save_education/',
                data:{
                    field_of_study:$('#field_of_study').val(),
                    new_field_of_study:$('#new_fos').val(),
                    grade:$('#grade').val(),
                    from_date:$('#from_date_eduction').val(),
                    to_date:$('#to_date_eduction').val(),
                    degree:$('#student_degree').val(),
                    institution:$('#student_institution').val(),
                    new_institution:$('#new_institution').val(),
                    description:$('#student_degree_description').val(),
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                },
                success:function(){
                   alert('Education added!');
                    window.location.reload(true);
                }
          });
});


$(document).on('click', '#update_education', function(){
        //alert($('#education_id').val());
        //url = '{% url "" %}'
        $.ajax({
                type:'POST',
                url:'/update_education/',
                data:{
                    id:$('#education_id').val(),
                    field_of_study:$('#field_of_study').val(),
                    new_field_of_study:$('#new_fos').val(),
                    grade:$('#grade').val(),
                    from_date:$('#from_date_eduction').val(),
                    to_date:$('#to_date_eduction').val(),
                    degree:$('#student_degree').val(),
                    institution:$('#student_institution').val(),
                    new_institution:$('#new_institution').val(),
                    description:$('#student_degree_description').val(),
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                },
                success:function(){
                   alert('Education added!');
                    window.location.reload(true);
                }
          });
});



$(document).on('click', '#add_publication', function(){
    alert('publication worked');
     $.ajax({
        type:'POST',
        url:'/add_publication/',
        data:{

            pub_title:$('#pub_title').val(),
            pub_publisher:$('#pub_publisher').val(),
            pub_date:$('#pub_date').val(),
            pub_url:$('#pub_url').val(),
            pub_description:$('#pub_description').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(){
            alert('Education added!');
            window.location.reload(true);
        }


     });

});

$(document).on('click', '#edit_publication', function(){
    $.ajax({
        type:'POST',
        url:'/edit_publication/',
        data:{
            id:$('#pub_id').val(),
            pub_title:$('#pub_title').val(),
            pub_publisher:$('#pub_publisher').val(),
            pub_date:$('#pub_date').val(),
            pub_url:$('#pub_url').val(),
            pub_description:$('#pub_description').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(){
            alert('Education edited!');
            window.location.reload(true);
        }


     });
});



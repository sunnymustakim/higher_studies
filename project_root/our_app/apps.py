from django.apps import AppConfig


class OurAppConfig(AppConfig):
    name = 'our_app'

    def ready(self):
        import our_app.signals

from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from our_app.models import Profile
from allauth.account.signals import user_signed_up, email_confirmed
from django.contrib.auth.models import User

from django.conf import settings
#from crum import get_current_request
import time
#User = get_user_model()

#receiver signal function to update the extra fields while a User instance is created

#@receiver(user_signed_up)
# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_profile(sender, instance, created, **kwargs):
#     if created:
#         #Profile.objects.get(id=instance.id).update(picture_url='some text')
#
#         #instance.picture_url = 'some text'
#         obj = Profile.objects.create(user=instance)
#         obj.picture_url = 'something'
#         print('this is the object: ', obj)
#         User = get_user_model()
#         user = User.objects.get(id=instance.id)
#
#         print('this is id: ', instance.id)
#         print(user)
#         #print(user.socialaccount_set.get(user_id=102))
#         print(user.socialaccount_set.all())
#         print(time.time())
#
#
#         #print(user.socialaccount_set.filter(provider='google')[0].extra_data)
#
# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def save_profile(sender, instance, **kwargs):
#     instance.picture_url = 'some text'
#     instance.profile.save()

from django.contrib.auth.signals import user_logged_in
@receiver(user_signed_up)
def do_stuff(sender, user, request, **kwargs):
    obj = Profile.objects.create(user=user)
    try:
        url = user.socialaccount_set.get(user_id=user.id).extra_data['picture']
    except:
        url = 'default image'
    obj.picture_social = str(url)
    user.profile.save()






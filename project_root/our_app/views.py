from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from .models import *
from django.contrib.auth.decorators import login_required
#from django.contrib.auth.models import User
from .models import User
from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.http import HttpResponse
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from our_app.tokens import account_activation_token
from django.db import connection
cursor = connection.cursor()



# Create your views here.
def index(request):
    if request.method == 'GET':
        #return render(request, 'social_login/loginuser.html')
        return render(request, 'our_app/sign-in.html')
    else:
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'our_app/sign-in.html', {'error':'Username and password did not match'})
        else:
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('home')

def signup(request):
    if request.method == 'GET':
        return render(request, 'our_app/sign-in.html')
    else:
        if request.POST['password'] == request.POST['repeat-password']:
            try:
                # django considers the first param as username, here, email is considered as username
                # since we want to authenticate via email not username
                User = get_user_model()
                user = User.objects.create_user(request.POST['email'],
                                                request.POST['email'],
                                                request.POST['password'])
                user.refresh_from_db() #this will load the Profile model
                user.first_name = request.POST['first_name']
                user.last_name = request.POST['last_name']
                user.role = request.POST['role']
                user.set_password(raw_password = request.POST['password'])
                #user.username = user.email
                user.is_active = False
                user.save()

                current_site = get_current_site(request)
                subject = 'Activate Your Account'
                message = render_to_string('our_app/registration/account_activation_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })

                user.email_user(subject, message)
                return render(request, 'our_app/registration/account_activation_sent.html')
                #return redirect('account_activation_sent')




                #login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                #return redirect('home')
            except IntegrityError:
                return render(request, 'our_app/sign-in.html', {'error':'That email has already been taken. Please choose a new email'})
        else:
            return render(request, 'our_app/sign-in.html', { 'error': 'Passwords did not match'})
        

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        User = get_user_model()
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        Profile.objects.create(user=user)
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        return redirect('home')
    else:
        return render(request, 'our_app/registration/account_activation_invalid.html')


@login_required
def home(request):
    if request.method == 'GET':
        return render(request, 'our_app/index.html')
    else:
        User.objects.filter(id=request.POST['id']).update(role=request.POST['role'])
        return redirect('home')
    

#def home(request):
#    #print("it's working...")
#    profs = Professor.objects.all()
#    students = Student.objects.all()

#    context = {'profs': profs, 'students': students}
#    return render(request, 'our_app/home.html', context)

def student(request, pk):
    student = Student.objects.get(id=pk)
    matched_offers = Offer.objects.filter(position__contains=student.position_for)
    context = {'student': student, 'matched_offers': matched_offers}
    return render(request, 'our_app/student.html', context)

def prof(request,pk):
    offer_by_this_prof = Offer.objects.get(professor_id=pk)
    matched_student = Student.objects.filter(position_for__contains=offer_by_this_prof.position).\
                      filter(cgpa__gte=offer_by_this_prof.min_cg_requirement)
    context = {'offer': offer_by_this_prof, 'matched_student': matched_student}
    return render(request, 'our_app/professor.html', context)


def profile(request):
    try:
        students = Student.objects.get(user_id=request.user.id)
        experiences = Experience.objects.filter(student_id=students.id)
        foss = FieldOfStudy.objects.all()
        institutions = Institution.objects.all()
        skills = Skill.objects.all()
        educations = Education.objects.filter(student_id=students.id)
        publications = Publication.objects.filter(user_id=request.user.id)
        cursor.execute("SELECT * FROM our_app_education left join our_app_institution on our_app_education.institution_id=our_app_institution.id "
                       "left join our_app_fieldofstudy on our_app_education.field_of_study_id=our_app_fieldofstudy.id where our_app_education.student_id=%s", [students.id])
        info_edu_ins_field = cursor.fetchall()
                
        cursor.execute("select id,skill from our_app_skill where id in (select skill_id from our_app_student_skill where student_id =%s)", [students.id])
        student_skills = cursor.fetchall()

        # for p in student_skills:
        #     print(p)

        countries = Country.objects.all()
        if students.country_id:
            country = Country.objects.get(id=students.country_id)
            context = {'students': students, 'experiences':experiences, 'institutions':institutions, 'educations':educations,'all_info':info_edu_ins_field, 'foss': foss,'countries':countries, 'country':country, 'student_skills':student_skills, 'skills': skills, 'publications':publications}
        else:
            context = {'students': students, 'experiences': experiences, 'institutions':institutions, 'educations':educations,'all_info':info_edu_ins_field, 'foss': foss,'countries':countries, 'student_skills':student_skills, 'skills': skills, 'publications':publications}
        # print(students.country_id)
        print('profile worked')

    except:
        print('into except block')
        context = {'students': 'no data'}
    return render(request, 'our_app/profile.html', context)

#def signup(request):
#    return render(request, 'our_app/signup.html')


def profile_submit(request):
    print('ajax worked')
    if request.method == 'POST':
        user_id = request.user.id
        overview = request.POST['overview']
        position = request.POST['position']


        print(user_id, overview, position)
        if Student.objects.filter(user_id=user_id):
            Student.objects.filter(user_id=user_id).update(research_interest=overview)
            Student.objects.filter(user_id=user_id).update(intended_position=position)
        else:
            Student.objects.create(
                user_id=user_id,
                research_interest=overview,
                intended_position=position
            )

        return HttpResponse('')

def contact_submit(request):
    print('ajax worked')
    if request.method == 'POST':
        user_id = request.user.id
        email = request.POST['email']
        phone = request.POST['phone']
        address = request.POST['address']
        country = request.POST['country']

        print(user_id, email, phone, address, country)
        if Student.objects.filter(user_id=user_id):
            Student.objects.filter(user_id=user_id).update(email=email)
            Student.objects.filter(user_id=user_id).update(phone=phone)
            Student.objects.filter(user_id=user_id).update(address=address)
            Student.objects.filter(user_id=user_id).update(country=country)
        else:
            Student.objects.create(
                user_id=user_id,
                email=email,
                phone=phone,
                address=address,
                country=country
            )

        return HttpResponse('')

def skill_submit(request):
    print('ajax worked')
    if request.method == 'POST':
        user_id = request.user.id
        skill_id = request.POST['skill_id']
        new_skill = request.POST['new_skill']

        if new_skill:
            skill = Skill.objects.create(skill=new_skill)
            skill_id = skill.id

        print('##########################################', user_id, skill_id)
        if Student.objects.filter(user_id=user_id):
            student_id = Student.objects.get(user_id=user_id).id
            student = Student.objects.get(id=student_id)
            student.skill.add(Skill.objects.get(id=skill_id))
        return HttpResponse('')


def skill_delete(request):
    print('ajax worked')
    if request.method == 'GET':
        user_id = request.user.id
        skill_id = request.GET['skill_id']

        print('##########################################', user_id, skill_id)
        if Student.objects.filter(user_id=user_id):
            student_id = Student.objects.get(user_id=user_id).id
            student = Student.objects.get(id=student_id)
            student.skill.remove(Skill.objects.get(id=skill_id))
    return HttpResponse('')

        
def experience_submit(request):
    if request.method == 'POST':
        user_id = request.user.id

        job_title = request.POST['job_title']
        job_description = request.POST['job_description']
        from_date = request.POST['from_date']
        to_date = request.POST['to_date']

        print('##########################################', user_id, job_title, job_description, from_date, to_date)
        if Student.objects.filter(user_id=user_id):
            student_id = Student.objects.get(user_id=user_id).id
            Experience.objects.create(
                title=job_title,
                description=job_description,
                from_date=from_date,
                to_date=to_date,
                student_id=student_id

            )
            #Student.objects.filter(user_id=user_id).update(research_interest=job_title)
            #Student.objects.filter(user_id=user_id).update(intended_position=job_description)
        else:
            Student.objects.create(user_id=user_id)
            student_id = Student.objects.get(user_id=user_id).id
            Experience.objects.create(
                title=job_title,
                description=job_description,
                from_date=from_date,
                to_date=to_date,
                student_id=student_id

            )

        return HttpResponse('')

def updateExperience(request):
    if request.method == 'POST':
        id = request.POST['id']
        job_title = request.POST['job_title']
        job_description = request.POST['job_description']
        from_date = request.POST['from_date']
        to_date = request.POST['to_date']

        Experience.objects.filter(id=id).update(title=job_title)
        Experience.objects.filter(id=id).update(description=job_description)
        Experience.objects.filter(id=id).update(from_date=from_date)
        Experience.objects.filter(id=id).update(to_date=to_date)

    return HttpResponse('')

def updateEducation(request):
    print('function worked')
    if request.method == 'POST':
        id = request.POST['id']
        field_of_study = request.POST['field_of_study']
        new_field_of_study = request.POST['new_field_of_study'] # <---------------
        grade = request.POST['grade']
        from_date = request.POST['from_date']
        to_date = request.POST['to_date']
        degree = request.POST['degree']
        description = request.POST['description']
        institution = request.POST['institution']
        new_institution = request.POST['new_institution'] # <--------------------

        if new_institution:
            institution = Institution.objects.create(institution_name=new_institution)
            institution = institution.id

        if new_field_of_study:
            field_of_study = FieldOfStudy.objects.create(major=new_field_of_study)
            field_of_study = field_of_study.id

        Education.objects.filter(id=id).update(degree=degree)
        Education.objects.filter(id=id).update(from_date=from_date)
        Education.objects.filter(id=id).update(to_date=to_date)
        Education.objects.filter(id=id).update(grade=grade)
        Education.objects.filter(id=id).update(description=description)
        Education.objects.filter(id=id).update(institution_id=institution)
        Education.objects.filter(id=id).update(field_of_study_id=field_of_study)

        #Education.objects.filter(id=id).update()
        #Education.objects.filter(id=id).update()
        print('###################', field_of_study)
    return HttpResponse('')


def education_submit(request):
    if request.method == 'POST':
        user_id = request.user.id
        field_of_study = request.POST['field_of_study']
        new_field_of_study = request.POST['new_field_of_study']
        grade = request.POST['grade']
        from_date = request.POST['from_date']
        to_date = request.POST['to_date']
        degree = request.POST['degree']
        description = request.POST['description']
        institution = request.POST['institution']
        new_institution = request.POST['new_institution']

        if new_institution:
            institution = Institution.objects.create(institution_name = new_institution)
            institution = institution.id

        if new_field_of_study:
            field_of_study = FieldOfStudy.objects.create(major=new_field_of_study)
            field_of_study = field_of_study.id

        if Student.objects.filter(user_id=user_id):
            student_id = Student.objects.get(user_id=user_id).id
            Education.objects.create(
                grade = grade,
                from_date = from_date,
                to_date = to_date,
                degree = degree,
                description = description,
                student_id=student_id,
                field_of_study_id = field_of_study,
                institution_id = institution
            )




        else:
            Student.objects.create(user_id=user_id)
            student_id = Student.objects.get(user_id=user_id).id
            Education.objects.create(
                grade=grade,
                from_date=from_date,
                to_date=to_date,
                degree=degree,
                description=description,
                student_id=student_id,
                field_of_study_id=field_of_study,
                institution_id=institution

            )

        print('####################################', field_of_study)


    return HttpResponse('')

def addPublication(request):
    if request.method == 'POST':
        user_id = request.user.id
        pub_title = request.POST['pub_title']
        pub_publisher = request.POST['pub_publisher']
        pub_date = request.POST['pub_date']
        pub_url = request.POST['pub_url']
        pub_description = request.POST['pub_description']

        Publication.objects.create(
            user_id = user_id,
            title = pub_title,
            year_published = pub_date,
            publisher = pub_publisher,
            description = pub_description,
            url = pub_url
        )

    print(user_id, pub_title, pub_publisher, pub_date, pub_url, pub_description)
    return HttpResponse('')

def editPublication(request):
    if request.method == 'POST':
        user_id = request.user.id

        id = request.POST['id']
        pub_title = request.POST['pub_title']
        pub_publisher = request.POST['pub_publisher']
        pub_date = request.POST['pub_date']
        pub_url = request.POST['pub_url']
        pub_description = request.POST['pub_description']

        Publication.objects.filter(id=id).update(title=pub_title)
        Publication.objects.filter(id=id).update(year_published=pub_date)
        Publication.objects.filter(id=id).update(publisher=pub_publisher)
        Publication.objects.filter(id=id).update(description=pub_description)
        Publication.objects.filter(id=id).update(url=pub_url)

    print(id, pub_title, pub_publisher, pub_date, pub_url, pub_description)
    return HttpResponse('')
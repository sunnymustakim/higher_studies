from django.urls import path, include
from our_app import views
from django.contrib.auth.views import LogoutView
from django.contrib.auth import views as auth_views
urlpatterns = [
    path('', views.index, name="index"),
    #path('', views.signup, name="signup"),
    path('home/', views.home, name="home"),
    #path('signin', views.signin, name='user_signin'),

    path('profile/', views.profile, name="profile"),



    path('submit/contact_submit', views.contact_submit, name="contact_submit"),

    path('submit/skill_submit', views.skill_submit, name="skill_submit"),

    path('submit/skill_delete', views.skill_delete, name="skill_delete"),

    path('student/<str:pk>/', views.student, name="student"),
    path('professor/<str:pk>/', views.prof, name="prof"),
    path('signup/', views.signup, name='signup'),
    path('logout/', LogoutView.as_view(), name='logout'),

    #url to redirect to google login
    path('accounts/', include('allauth.urls')),

    #password reset url
    path('reset_password/', auth_views.PasswordResetView.as_view
        (template_name="our_app/registration/password_reset.html"), name='reset_password'),

    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view
        (template_name="our_app/registration/password_reset_sent.html"), name='password_reset_done'),

    #template_name="our_app/registration/password_reset_form.html"
    path('reset/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),

    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view
        (template_name="our_app/registration/password_reset_done.html"), name='password_reset_complete'),

    path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
         views.activate, name='activate'),

    # submit overview and position from profile.html page via AJAX
    path('submit/', views.profile_submit, name="profile_submit"),

    # submit experience from profile.html page via AJAX
    path('submit_experience/', views.experience_submit, name="experience_submit"),

    path('update_experience/', views.updateExperience, name="update_experience"),

    path('save_education/', views.education_submit, name="education_submit"),

    path('update_education/', views.updateEducation, name="update_education"),

    path('add_publication/', views.addPublication, name="add_publication"),

    path('edit_publication/', views.editPublication, name="edit_publication"),

]
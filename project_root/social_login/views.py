from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError

from social_login.forms import SignUpForm

# Create your views here.
def login_social(request):
    return render(request, 'social_login/login.html')

def signup(request):
    if request.method == 'GET':
        return render(request, 'social_login/signup.html', {'form': SignUpForm()})
    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.create_user(request.POST['username'],
                                                request.POST['email'],
                                                request.POST['password1'])
                user.refresh_from_db() #this will load the Profile model
                user.first_name = request.POST['first_name']
                user.last_name = request.POST['last_name']
                user.profile.role = request.POST['role']
                user.set_password(raw_password = request.POST['password1'])
                user.save()
                login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                return redirect('login_social')
            except IntegrityError:
                return render(request, 'social_login/signup.html', {'form':SignUpForm(), 'error':'That username has already been taken. Please choose a new username'})
        else:
            return render(request, 'social_login/signup.html', {'form': SignUpForm(), 'error': 'Passwords did not match'})

def signin(request):
    if request.method == 'GET':
        return render(request, 'social_login/loginuser.html', {'form':AuthenticationForm()})
    else:
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'social_login/loginuser.html', {'form':AuthenticationForm(), 'error':'Username and password did not match'})
        else:
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('login_social')




from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth import get_user_model

#class to override the default django password reset form, this is incomplete and needs more work
class ResetPassword(PasswordResetForm):
    class Meta:
        fields = ("new_password1", "new_password2")
        model = get_user_model()
        help_texts = {
            'new_password1': '',
            'new_password2': '',
        }



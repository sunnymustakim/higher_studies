$(window).on("load", function() {
    "use strict";

    //  ============= IMAGE UPLOAD FORM FUNCTION =========


    //  ============= POST PROJECT POPUP FUNCTION =========

    $(".post_project").on("click", function(){
        $(".post-popup.pst-pj").addClass("active");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".post-project > a").on("click", function(){
        $(".post-popup.pst-pj").removeClass("active");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= POST JOB POPUP FUNCTION =========

    $(".post-jb").on("click", function(){
        $(".post-popup.job_post").addClass("active");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".post-project > a").on("click", function(){
        $(".post-popup.job_post").removeClass("active");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= SIGNIN CONTROL FUNCTION =========

    $('.sign-control li').on("click", function(){
        var tab_id = $(this).attr('data-tab');
        $('.sign-control li').removeClass('current');
        $('.sign_in_sec').removeClass('current');
        $(this).addClass('current animated fadeIn');
        $("#"+tab_id).addClass('current animated fadeIn');
        return false;
    });

    //  ============= SIGNIN TAB FUNCTIONALITY =========

    $('.signup-tab ul li').on("click", function(){
        var tab_id = $(this).attr('data-tab');
        $('.signup-tab ul li').removeClass('current');
        $('.dff-tab').removeClass('current');
        $(this).addClass('current animated fadeIn');
        $("#"+tab_id).addClass('current animated fadeIn');
        return false;
    });

    //  ============= SIGNIN SWITCH TAB FUNCTIONALITY =========

    $('.tab-feed ul li').on("click", function(){
        var tab_id = $(this).attr('data-tab');
        $('.tab-feed ul li').removeClass('active');
        $('.product-feed-tab').removeClass('current');
        $(this).addClass('active animated fadeIn');
        $("#"+tab_id).addClass('current animated fadeIn');
        return false;
    });

    //  ============= COVER GAP FUNCTION =========

    var gap = $(".container").offset().left;
    $(".cover-sec > a, .chatbox-list").css({
        "right": gap
    });

    //  ============= OVERVIEW EDIT FUNCTION =========

    $(".overview-open").on("click", function(){
        $("#overview-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;

    });
    $(".close-box").on("click", function(){
        $("#overview-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= EXPERIENCE EDIT FUNCTION =========

    $(".exp-bx-open").on("click", function(){
        //clean form
        $("#job_title").val('');
        $("#job_description").val('');

        //show save button and hide update button if adding experience
        $("#save_experience").show();
        $("#update_experience").hide();

        //condition to check if it's experience add or update
        if ($(this).attr("href") != '#'){

            //show update button and hide save button if condition returned true
            $("#save_experience").hide();
            $("#update_experience").show();

            //fetch experience information from html to display on edit form
            var exp = $(this).attr("value");
            // split on '|' since all the information are piped in html tag
            var all = exp.split('|');

            //information needed to edit form
            var id = all[0];
            var title = all[1];
            var description = all[2];
            var from_date = all[3];
            var to_date = all[4];

            //process date as html expects to write back into html
            var from_d1 = Date.parse(from_date);
            var from_d2 = new Date(from_d1);
            var from_dd = String(from_d2.getDate()).padStart(2, '0');
            var from_mm = String(from_d2.getMonth() + 1).padStart(2, '0');
            var from_yyyy = String(from_d2.getFullYear());
            var from_date_final = from_yyyy + '-' + from_mm + '-' + from_dd;

            var to_d1 = Date.parse(to_date);
            var to_d2 = new Date(to_d1);
            var to_dd = String(to_d2.getDate()).padStart(2, '0');
            var to_mm = String(to_d2.getMonth() + 1).padStart(2, '0');
            var to_yyyy = String(to_d2.getFullYear());
            var to_date_final = to_yyyy + '-' + to_mm + '-' + to_dd;

            //write back into html form
            $("#experience_id").val(id);
            $("#job_title").val(title);
            $("#job_description").val(description);
            $("#from_date").val(from_date_final);
            $("#to_date").val(to_date_final);


        }

        //show form
        $("#experience-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function(){
        $("#experience-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });


    //  ============= PUBLICATION EDIT FUNCTION =========

    $(".pub-bx-open").on("click", function(){

        $("#add_publication").show();
        $("#edit_publication").hide();

        if ($(this).attr("href") != '#'){
            $("#add_publication").hide();
            $("#edit_publication").show();

            var pub = $(this).attr("value");
            var all = pub.split('|');

            var id = all[0];
            var title = all[1];
            var publisher = all[2];
            var year_published = all[3];
            var url = all[4]
            var description = all[5];

            //alert(id);
            //process date as html expects to write back into html
            var date1 = Date.parse(year_published);
            var date2 = new Date(date1);
            var dd = String(date2.getDate()).padStart(2, '0');
            var mm = String(date2.getMonth() + 1).padStart(2, '0');
            var yyyy = String(date2.getFullYear());
            var date_final = yyyy + '-' + mm + '-' + dd;

            //write back into html form
            $("#pub_title").val(title);
            $("#pub_id").val(id);
            $("#pub_publisher").val(publisher);
            $("#pub_date").val(date_final);
            $("#pub_url").val(url);
            $("#pub_description").val(description);

        }


        $("#publication-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function(){
        $("#publication-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });



    //  ============= EDUCATION EDIT FUNCTION =========

    $(".ed-box-open").on("click", function(){
        $("#save_education").show();
        $("#update_education").hide();
        if ($(this).attr("href") != '#'){
            //alert($(this).attr("href"))
            $("#save_education").hide();
            $("#update_education").show();
            var edu = $(this).attr("value");
            var all = edu.split('|');

            var id = all[8];
            var institution = all[2];
            var institution_id = all[9];
            var fos = all[1];
            var fos_id = all[10];
            var grade = all[7];
            var from_date = all[4];
            var to_date = all[5];
            var degree = all[0]
            var description = all[6];


            //process date as html expects to write back into html
            var from_d1 = Date.parse(from_date);
            var from_d2 = new Date(from_d1);
            var from_dd = String(from_d2.getDate()).padStart(2, '0');
            var from_mm = String(from_d2.getMonth() + 1).padStart(2, '0');
            var from_yyyy = String(from_d2.getFullYear());
            var from_date_final = from_yyyy + '-' + from_mm + '-' + from_dd;

            var to_d1 = Date.parse(to_date);
            var to_d2 = new Date(to_d1);
            var to_dd = String(to_d2.getDate()).padStart(2, '0');
            var to_mm = String(to_d2.getMonth() + 1).padStart(2, '0');
            var to_yyyy = String(to_d2.getFullYear());
            var to_date_final = to_yyyy + '-' + to_mm + '-' + to_dd;

            //write back into html form
            $("#education_id").val(id);
            $("#student_institution").val(institution_id);
            $("#field_of_study").val(fos_id);
            $("#grade").val(grade);
            $("#from_date_eduction").val(from_date_final);
            $("#to_date_eduction").val(to_date_final);
            $("#student_degree").val(degree);
            $("#student_degree_description").val(description);

        }
        $("#education-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });


    $(".close-box").on("click", function(){
        $("#education-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    // ============= INPUT ON CHANGE FUNCTION =========

    $('#new_institution').hide(500);
    $('#student_institution').change(function(){
      if($(this).val() == 'other_selected'){
        $('#new_institution').show(500);
      }else{
        $('#new_institution').hide(500);
      }

    });

    $('#new_fos').hide(500);
    $('#field_of_study').change(function(){
      if($(this).val() == 'other_fos_selected'){
        $('#new_fos').show(500);
      }else{
        $('#new_fos').hide(500);
      }

    });


    // ============= New skill adding option =========

    $('#new_skill').hide(500);
    $('#student_skill').change(function(){
      if($(this).val() == 'other_skill'){
        $('#new_skill').show(500);
      }else{
        $('#new_skill').hide(500);
      }

    });


    //  ============= LOCATION EDIT FUNCTION =========


    $(".lct-box-open").on("click", function(){
        $("#location-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function(){
        $("#location-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= SKILLS EDIT FUNCTION =========

    $(".skills-open").on("click", function(){
        $("#skills-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function(){
        $("#skills-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= ESTABLISH EDIT FUNCTION =========

    $(".esp-bx-open").on("click", function(){
        $("#establish-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function(){
        $("#establish-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= CREATE PORTFOLIO FUNCTION =========

    // $(".portfolio-btn > a").on("click", function(){
    //     $("#create-portfolio").addClass("open");
    //     $(".wrapper").addClass("overlay");
    //     return false;
    // });
    // $(".close-box").on("click", function(){
    //     $("#create-portfolio").removeClass("open");
    //     $(".wrapper").removeClass("overlay");
    //     return false;
    // });

    //  ============= EMPLOYEE EDIT FUNCTION =========

    $(".emp-open").on("click", function(){
        $("#total-employes").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function(){
        $("#total-employes").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  =============== Ask a Question Popup ============

    $(".ask-question").on("click", function(){
        $("#question-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function(){
        $("#question-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });


    //  ============== ChatBox ============== 


    $(".chat-mg").on("click", function(){
        $(this).next(".conversation-box").toggleClass("active");
        return false;
    });
    $(".close-chat").on("click", function(){
        $(".conversation-box").removeClass("active");
        return false;
    });

    //  ================== Edit Options Function =================


    $(".ed-opts-open").on("click", function(){
        $(this).next(".ed-options").toggleClass("active");
        return false;
    });


    // ============== Menu Script =============

    $(".menu-btn > a").on("click", function(){
        $("nav").toggleClass("active");
        return false;
    });


    //  ============ Notifications Open =============

    $(".not-box-open").on("click", function(){$("#message").hide();
        $(".user-account-settingss").hide();
        $(this).next("#notification").toggle();
    });

     //  ============ Messages Open =============

    $(".not-box-openm").on("click", function(){$("#notification").hide();
        $(".user-account-settingss").hide();
        $(this).next("#message").toggle();
    });


    // ============= User Account Setting Open ===========
	/*
$(".user-info").on("click", function(){$("#users").hide();
        $(".user-account-settingss").hide();
        $(this).next("#notification").toggle();
    });
    
	*/
	$( ".user-info" ).click(function() {
  $( ".user-account-settingss" ).slideToggle( "fast");
	  $("#message").not($(this).next("#message")).slideUp();
	  $("#notification").not($(this).next("#notification")).slideUp();
    // Animation complete.
  });
 

    //  ============= FORUM LINKS MOBILE MENU FUNCTION =========

    $(".forum-links-btn > a").on("click", function(){
        $(".forum-links").toggleClass("active");
        return false;
    });
    $("html").on("click", function(){
        $(".forum-links").removeClass("active");
    });
    $(".forum-links-btn > a, .forum-links").on("click", function(){
        e.stopPropagation();
    });

    //  ============= PORTFOLIO SLIDER FUNCTION =========

    $('.profiles-slider').click({
        slidesToShow: 3,
        slck:true,
        slidesToScroll: 1,
        prevArrow:'<span class="slick-previous"></span>',
        nextArrow:'<span class="slick-nexti"></span>',
        autoplay: true,
        dots: false,
        autoplaySpeed: 2000,
        responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]


    });





});



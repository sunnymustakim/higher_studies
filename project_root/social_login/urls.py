from django.urls import path, include
from . import views
from django.contrib.auth.views import LogoutView



urlpatterns = [
    path('', views.login_social, name="login_social"),
    #path('accounts/', include('allauth.urls')),
    #path('', TemplateView.as_view(template_name="social_login/login.html")),
    #path('logout/', LogoutView.as_view(), name='logout'),
    #path('signup', views.signup, name='signup'),
    path('signin', views.signin, name='signin'),

]